package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="Car")
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "propSeq")
    @SequenceGenerator(name = "propSeq", sequenceName = "property_sequence", allocationSize = 1)

    @Column(name = "number")
    private String number;
    @Column(name = "brand")
    private String brand;
    @Column(name = "condition")
    private String condition;

    @GeneratedValue
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) { this.number = number; }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
