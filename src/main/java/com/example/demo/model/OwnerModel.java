package com.example.demo.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="User")
public class OwnerModel {
    @Column(name = "name")
    private String name;
    @Column(name = "phone")
    private String phone;
    @Column(name = "email")
    private String email;
    @Column(name = "car")
    private CarModel car;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "propSeq")
    @SequenceGenerator(name = "propSeq", sequenceName = "property_sequence", allocationSize = 1)


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CarModel getCar() { return car; }

    public void setCar(CarModel car) { this.car = car; }
}
