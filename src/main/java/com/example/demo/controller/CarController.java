package com.example.demo.controller;


import com.example.demo.model.CarModel;
import com.example.demo.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("Car")
@RequiredArgsConstructor
public class CarController {

    public final CarService carService;

    @GetMapping("/Get")
    public CarModel getModel(@RequestBody String number) {
        return carService.getModel(number);
    }

    @PostMapping("/Add")
    public void add(@RequestBody CarModel model) { carService.add(model); }

    @GetMapping("/list")
    public List<CarModel> getList() { return carService.getList(); }

    @PatchMapping("/Edit/{id}")
    public void edit(@RequestBody CarModel model) {
        carService.edit(model);
    }
}
