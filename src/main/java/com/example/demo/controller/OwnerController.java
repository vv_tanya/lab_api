package com.example.demo.controller;

import com.example.demo.model.OwnerModel;
import com.example.demo.service.OwnerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("Owner")
@RequiredArgsConstructor
public class OwnerController {

    @Autowired
    OwnerService ownerService;

    // просмотр пользователя по id
    @GetMapping("/Get")
    public OwnerModel getModel(@RequestBody String name) {
        return ownerService.getModel(name);
    }

    // добавление пользователя
    @PostMapping("/Add")
    public void add(@RequestBody OwnerModel model) { ownerService.add(model); }

    // просмотр всего списка пользователя
    @GetMapping("/list")
    public List<OwnerModel> getList() { return ownerService.getList(); }

    // редактирование пользователя
    @PatchMapping("/Edit/{id}")
    public void edit(@RequestBody OwnerModel model) {
        ownerService.edit(model);
    }
}
