package com.example.demo.repository;
import com.example.demo.model.CarModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CarRepository {
    private List<CarModel> storage = new ArrayList<>();

    public void add(CarModel model) {
        int count = 0;
        for (CarModel um : storage){
            count++;
            if (um.getNumber() == model.getNumber())
                break;
        }
        if (count==storage.size())
            storage.add(model);
    }

    public List<CarModel> getList() {
        return storage;
    }

    public CarModel getModel(String number) {
        for (CarModel um : storage){
            if (um.getNumber() == number)
                return um;
        }
        return null;
    }

    public void edit(CarModel model) {
        CarModel usermodel = null;
        for (CarModel um : storage){
            if (um.getNumber() == model.getNumber())
                usermodel = um;
        }

        if (usermodel != null || usermodel.getNumber()!=model.getNumber()) {

            if (usermodel.getBrand() != null) {
                usermodel.setBrand(model.getBrand());
            }

            if (usermodel.getCondition() != null) {
                usermodel.setCondition(model.getCondition());
            }
        }
    }
}
