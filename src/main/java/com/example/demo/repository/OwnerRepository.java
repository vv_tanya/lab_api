package com.example.demo.repository;
import com.example.demo.model.OwnerModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OwnerRepository {
    private List<OwnerModel> storage = new ArrayList<>();

    public void add(OwnerModel model) {
        int count = 0;
        for (OwnerModel um : storage){
            count++;
            if (um.getName() == model.getName())
                break;
        }
        if (count==storage.size())
            storage.add(model);
    }

    public List<OwnerModel> getList() {
        return storage;
    }

    public OwnerModel getModel(String name) {
        for (OwnerModel um : storage){
            if (um.getName() == name)
                return um;
        }
        return null;
    }

    public void edit(OwnerModel model) {
        OwnerModel usermodel = null;
        for (OwnerModel um : storage){
            if (um.getName() == model.getName())
                usermodel = um;
        }

        if (usermodel != null || usermodel.getName()!=model.getName()) {

            if (usermodel.getPhone() != null) {
                usermodel.setPhone(model.getPhone());
            }

            if (usermodel.getEmail() != null) {
                usermodel.setEmail(model.getEmail());
            }

            if (usermodel.getCar() != null) {
                usermodel.setCar(model.getCar());
            }
        }
    }

}
