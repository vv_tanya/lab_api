package com.example.demo.service;

import com.example.demo.model.CarModel;
import com.example.demo.model.OwnerModel;
import com.example.demo.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CarService {
    @Autowired
    CarRepository carRepository;

    public void add (CarModel model) { carRepository.add(model); }

    public void edit(CarModel model) {
        carRepository.edit(model);
    }

    public List<CarModel> getList() {
        return carRepository.getList();
    }

    public CarModel getModel(String number) {
        return carRepository.getModel(number);
    }

}
