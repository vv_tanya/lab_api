package com.example.demo.service;

import com.example.demo.model.OwnerModel;
import com.example.demo.repository.OwnerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class OwnerService {

    OwnerRepository ownerRepository;

    public void add(OwnerModel model) { ownerRepository.add(model); }

    public void edit(OwnerModel model) {
        ownerRepository.edit(model);
    }

    public OwnerModel getModel(String name) {
        return ownerRepository.getModel(name);
    }

    public List<OwnerModel> getList() {
        return ownerRepository.getList();
    }

}
